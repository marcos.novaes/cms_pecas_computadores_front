import React from 'react'
import { Router, Switch, Route, Redirect } from 'react-router-dom'
import { Home, Dash } from './views/index'
import history from './config/history'
import { isAuthenticated } from './config/auth'
import Login from './components/auth/login'
import LayoutAdmin from './components/layout/admin/index'
import Produtos from './components/admin/produtos'
import Info from './components/admin/info'
import Banner from './components/admin/banner'
import Servicos from './components/admin/servicos'

const Routes = () => {

    const AuthenticatedRoute = ({ ...rest }) => {
        if (!isAuthenticated()) {
            return <Redirect to='/login' />
        }
        return <Route {...rest} />
    }

    return (
        <Router history={history}>
            <Switch>
                <Route exact path='/login' component={Login}></Route>
                <Route exact path='/' component={Home}></Route>
                <LayoutAdmin>
                    <AuthenticatedRoute exact path='/admin' component={() => <p>Home</p>}></AuthenticatedRoute>
                    <AuthenticatedRoute exact path='/admin/produtos' component={Produtos}></AuthenticatedRoute>
                    <AuthenticatedRoute exact path='/admin/servicos' component={Servicos}></AuthenticatedRoute>
                    <AuthenticatedRoute exact path='/admin/banner' component={Banner}></AuthenticatedRoute>
                    <AuthenticatedRoute exact path='/admin/info' component={Info}></AuthenticatedRoute>
                </LayoutAdmin>
            </Switch>
        </Router>
    )
}


export default Routes;