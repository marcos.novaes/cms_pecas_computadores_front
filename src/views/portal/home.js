import React, { useEffect, useState } from 'react';
import Layout from '../../components/layout/portal/index';
import styled from 'styled-components';
import { getInfos, getServices, getProducts, getBanners } from '../../services/admin'

const Portal = (props) => {
    const [infos, setInfos] = useState([])
    const [services, setServices] = useState([])
    const [products, setProducts] = useState([])
    const [banners, setBanners] = useState([])


    const setItems = async () => {
        try {
            let resultInfo = await getInfos()
            setInfos(resultInfo.data)
            console.log(resultInfo.data);
            let resultService = await getServices()
            setServices(resultService.data)
            console.log(resultService.data);
            let resultProducts = await getProducts()
            setProducts(resultProducts.data)
            console.log(resultProducts.data);
            let resultBanner = await getBanners()
            setBanners(resultBanner.data)
            console.log(resultBanner.data);
        } catch (error) {
            console.log(error.message)
        }
    }

    useEffect(() => {
        setItems()
        return () => { }
    }, [])

    return (
        <Layout>
            <Banner />
            <Services />
            <Infos />
            <Products />
        </Layout>
    );




}

const Banner = styled.div`
    width: 100%;
    background-color: red;
    height: 300px;
`

const Services = styled.div`
    width: 100%;
    background-color: green;
    height: 100px;  
`

const Infos = styled.div`
    width: 100%;
    background-color: yellow;
    height: 250px;  
`

const Products = styled.div`
    width: 100%;
    background-color: purple;
    height: 400px;  
`


export default Portal;

