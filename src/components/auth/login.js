import React, { useState } from 'react'
import styled from 'styled-components';
import { Form, Spinner, Button } from 'react-bootstrap';
import { FaHome } from 'react-icons/fa'
import { Link } from 'react-router-dom';
import { authentication } from '../../services/auth'
import { saveToken } from '../../config/auth'
import clientHttp from '../../config/clientHttp'
import history from '../../config/history'

export default () => {

    const [form, setForm] = useState({})


    const handleChange = (e) => {
        setForm({
            ...form,
            [e.target.name]: e.target.value
        })
    }


    const handleSubmit = async (e) => {
        e.preventDefault()
        console.log(form)
        try {
            const { data } = await authentication(form)
            const { token } = data
            clientHttp.defaults.headers["x-auth-token"] = token;
            saveToken(token)
            history.push('/admin')
        } catch (error) {
            console.log(error.message)
        }
    }

    return (<Login>
        <div className="row justify-content-center">
            <div className="col-xl-10 col-lg-12 col-md-9 col-sm-6">
                <Card>
                    <div className="card-body p-5">
                        <div>
                            <h3>Acesso ao Painel de Admin</h3>
                        </div>
                        <Form className="user">
                            <Form.Group controlId="formBasicEmail">
                                <Form.Control className="form-control form-control-user" type="email" name="email" placeholder="Enter email" value={form.email || ""} onChange={(e) => handleChange(e)} />
                            </Form.Group>

                            <Form.Group controlId="formBasicPassword">
                                <Form.Control className="form-control form-control-user" type="password" name="password" value={form.password || ""} placeholder="Password" onChange={(e) => handleChange(e)} />
                            </Form.Group>

                            <Button className='btn-primary large' onClick={(e) => handleSubmit(e)} variant="primary" type="submit" block>
                                {/* <Spinner
                                    as="span"
                                    animation="grow"
                                    size="sm"
                                    role="status"
                                    aria-hidden="true"
                                />
                                                        Carregando... */}
                                                        Entrar...
                            </Button>
                        </Form>
                        <hr />
                        <div className="text-center">
                            <Link className="small" to="/">
                                <FaHome /> Pagina Inicial
                                            </Link>
                        </div>
                    </div>
                </Card>
            </div>
        </div>
    </Login>)
}



const Login = styled.div.attrs({
    className: 'container',
})`
    margin-top: 20px;
    background-color: #6bb9f0;
    display: flex;
    justify-content: center;
    align-items: center;
    height: 600px;
`

const Card = styled.div.attrs({
    className: 'card',
})`
    width: 500px;
`