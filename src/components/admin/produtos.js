import React, { useState, useEffect } from 'react'
import { Container, Form, Button, Table, Image } from 'react-bootstrap'
import FormProduct from './form/product'
import { getProducts } from '../../services/admin'
import TableProduct from './table/product'



const Produtos = (props) => {

    const [showTable, setShowTable] = useState(true)

    const [products, setProducts] = useState([])

    const [prodAtual, setProdAtual] = useState({})


    const handleCLickShowTable = () => {
        setProdAtual({})
        setShowTable(!showTable)
    }

    useEffect(() => {
        let pegarProdutos = async () => {
            try {
                const prods = await getProducts()
                setProducts(prods.data)
            } catch (error) {
                console.log(error)
            }
        }
        pegarProdutos()

        return () => { }
    }, [showTable])

    return (
        <>
            <Container>
                <Button className="mb-4 px-4" variant="outline-success" size="lg" onClick={() => handleCLickShowTable()}>{showTable ? "Criar" : "Ver"}</Button>
                {showTable ?
                    <TableProduct products={products} showT={setShowTable} setProd={setProdAtual} ></TableProduct> :
                    <FormProduct prod={prodAtual} showT={setShowTable}></FormProduct>
                }


            </Container>


        </>

    )
}


export default Produtos

