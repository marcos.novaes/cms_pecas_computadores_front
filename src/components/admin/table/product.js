import React, { useState, useEffect } from 'react'
import { Table } from "react-bootstrap";
import styled from 'styled-components';

const TableProduct = ({ products, showT, setProd }) => {

    const [prods, setProds] = useState([])

    const trocaStatus = (status) => {
        const stats = status ? "Ativo" : "Inativo"
        return stats
    }

    const formataPreco = (preco) => {
        return `R$ ${preco}`
    }

    const handleShowEdit = (e, prod) => {
        e.preventDefault()
        setProd(prod)
        showT(false)
    }

    const montaLinhasTabela = (prods) => {
        return (
            <tbody>
                {prods.map((prod, i) => {
                    return (<tr key={i}>
                        <Td>{trocaStatus(prod.status)}</Td>
                        <Td>{prod.title}</Td>
                        <Td>{formataPreco(prod.price)}</Td>
                        <Td><button onClick={(e) => handleShowEdit(e, prod)}>Editar</button>| <button>Excluir</button></Td>
                    </tr>)
                })}
            </tbody>
        )
    }


    useEffect(() => {
        setProds(products)
    }, [products])

    return (
        <>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <Th>Status</Th>
                        <Th>Titulo</Th>
                        <Th>Preço</Th>
                        <Th>Ações</Th>
                    </tr>
                </thead>
                {montaLinhasTabela(prods)}
            </Table>
        </>
    )
}

const Th = styled.th`
    text-align: center;
`
const Td = styled.td`
    text-align: center;
`


export default TableProduct