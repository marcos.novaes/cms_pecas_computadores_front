import React, { useState, useEffect } from 'react'
import { Table } from "react-bootstrap";
import styled from 'styled-components';

const TableService = ({ servics, showT, setService }) => {

    const [services, setServices] = useState([])



    const handleShowEdit = (e, service) => {
        e.preventDefault()
        showT(false)
        setService(service)
    }

    const montaLinhasTabela = (services) => {
        return (
            <tbody>
                {services.map((service, i) => {
                    return (<tr key={i}>
                        <Td>{service.name}</Td>
                        <Td>{service.description}</Td>
                        <Td>{service.icon}</Td>
                        <Td><button onClick={(e) => handleShowEdit(e, service)}>Editar</button>| <button>Excluir</button></Td>
                    </tr>)
                })}
            </tbody>
        )
    }


    useEffect(() => {
        setServices(servics)
    }, [servics])

    return (
        <>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <Th>Nome</Th>
                        <Th>Descriçao</Th>
                        <Th>icone</Th>
                    </tr>
                </thead>
                {montaLinhasTabela(services)}
            </Table>
        </>
    )
}

const Th = styled.th`
    text-align: center;
`
const Td = styled.td`
    text-align: center;
`


export default TableService