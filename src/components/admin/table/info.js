import React, { useState, useEffect } from 'react'
import { Table } from "react-bootstrap";
import styled from 'styled-components';

const TableInfo = ({ infs, showT, setInfo }) => {

    const [infos, setInfos] = useState([])



    const handleShowEdit = (e, info) => {
        e.preventDefault()
        showT(false)
        setInfo(info)
    }

    const montaLinhasTabela = (infos) => {
        return (
            <tbody>
                {infos.map((info, i) => {
                    return (<tr key={i}>
                        <Td>{info.name}</Td>
                        <Td>{info.description}</Td>
                        <Td>{info.icon}</Td>
                        <Td><button onClick={(e) => handleShowEdit(e, info)}>Editar</button>| <button>Excluir</button></Td>
                    </tr>)
                })}
            </tbody>
        )
    }


    useEffect(() => {
        setInfos(infs)
    }, [infs])

    return (
        <>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <Th>Nome</Th>
                        <Th>Descriçao</Th>
                        <Th>icone</Th>
                    </tr>
                </thead>
                {montaLinhasTabela(infos)}
            </Table>
        </>
    )
}

const Th = styled.th`
    text-align: center;
`
const Td = styled.td`
    text-align: center;
`


export default TableInfo;