import React, { useState, useEffect } from 'react'
import { Table } from "react-bootstrap";
import styled from 'styled-components';

const TableBanner = ({ banners, showT, setBan }) => {

    const [bannes, setBannes] = useState([])


    const handleShowEdit = (e, ban) => {
        e.preventDefault()
        setBan(ban)
        showT(false)
    }

    const montaLinhasTabela = (bans) => {
        return (
            <tbody>
                {bans.map((ban, i) => {
                    return (<tr key={i}>
                        <Td>{ban.title}</Td>
                        <Td>{ban.description}</Td>
                        <Td><button onClick={(e) => handleShowEdit(e, ban)}>Editar</button>| <button>Excluir</button></Td>
                    </tr>)
                })}
            </tbody>
        )
    }


    useEffect(() => {
        setBannes(banners)
    }, [banners])

    return (
        <>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <Th>Titulo</Th>
                        <Th>Descriçao</Th>
                        <Th>Ações</Th>
                    </tr>
                </thead>
                {montaLinhasTabela(banners)}
            </Table>
        </>
    )
}

const Th = styled.th`
    text-align: center;
`
const Td = styled.td`
    text-align: center;
`


export default TableBanner