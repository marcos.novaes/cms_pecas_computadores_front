import React, { useState, useEffect } from 'react'
import { Container, Form, Button, Table } from 'react-bootstrap'
import FormBanner from './form/banner'
import TableBanner from './table/banner'
import { getBanners } from '../../services/admin'

const Banner = (props) => {
    const [showTable, setShowTable] = useState(true)

    const [banners, setBanners] = useState([])

    const [banAtual, setBanAtual] = useState({})


    const handleCLickShowTable = () => {
        setBanAtual({})
        setShowTable(!showTable)
    }

    useEffect(() => {
        let pegarBanners = async () => {
            try {
                const bans = await getBanners()
                setBanners(bans.data)
            } catch (error) {
                console.log(error)
            }
        }
        pegarBanners()

        return () => { }
    }, [showTable])

    return (
        <>
            <Container>
                <Button className="mb-4 px-4" variant="outline-success" size="lg" onClick={() => handleCLickShowTable()}>{showTable ? "Criar" : "Ver"}</Button>
                {showTable ?
                    <TableBanner banners={banners} showT={setShowTable} setBan={setBanAtual} ></TableBanner> :
                    <FormBanner ban={banAtual} showT={setShowTable} ></FormBanner>
                }


            </Container>


        </>

    )
}

export default Banner