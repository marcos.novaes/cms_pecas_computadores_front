import React, { useState, useEffect } from 'react'
import { Form, Button, InputGroup, Row, Col } from 'react-bootstrap'
import styled from 'styled-components';
import { createBanner, updateBanner } from '../../../services/admin'

const FormBanner = ({ ban, showT }) => {

    const [form, setForm] = useState({})
    const [updatePhoto, setUpdatePhoto] = useState(false)
    const [imgPreview, setImgPreview] = useState()
    const [isUpdate, setIsUpdate] = useState(false)

    const typeReq = (data) => isUpdate ? updateBanner(ban._id, data) : createBanner(data)




    const handleChange = (e) => {

        setForm({
            ...form,
            [e.target.name]: e.target.value
        })
        return;
    }

    const onImageChange = (event) => {
        if (event.target.files && event.target.files[0]) {
            let reader = new FileReader();
            let file = event.target.files[0];
            reader.onloadend = () => {
                setForm({
                    ...form,
                    'photo': file
                });
                setImgPreview(reader.result)
                setUpdatePhoto(true)
            };
            reader.readAsDataURL(file);
        }
    };


    const isNotValid = () => {
        return !(form.title && form.description && form.photo)
    }


    const buttonText = () => {
        const text = isUpdate ? "Atualizar" : "Criar"
        return text
    }

    const removePhoto = () => {
        setUpdatePhoto(false)
        setForm({
            ...form,
            photo: ""
        })
    }

    const clearForm = () => {
        setForm({})
        setImgPreview("")
        setUpdatePhoto(false)
    }

    const submitForm = (e) => {
        e.preventDefault()

        let data = new FormData()


        Object.keys(form)
            .forEach(key => data.append(key, form[key]))

        const config = {
            headers: {
                'Content-type': 'multipart/form-data'
            }
        }

        typeReq(data, config)
            .then((res) => {
                clearForm()
                showT(true)
            })
            .catch((err) => console.log(`Erro ao cadastrar banner.`))
    }


    useEffect(() => {

        if (Object.keys(ban).length > 0) {
            setIsUpdate(true)
            setForm(ban)
            setImgPreview(ban.photo)
            setUpdatePhoto(true)
        }

    }, [ban])

    return (<>
        <Form>
            <Form.Group controlId="formBasicTitle">
                <Form.Label>Titulo</Form.Label>
                <Form.Control type="text" placeholder="Digite o nome" name="title" value={form.title || ""} onChange={(e) => handleChange(e)} />
            </Form.Group>

            <Form.Group controlId="formBasicSDescription">
                <Form.Label>Descrição</Form.Label>
                <Form.Control type="text" placeholder="Digite uma descrição do banner" name="description" value={form.description || ""} onChange={(e) => handleChange(e)} />
            </Form.Group>

            <Form.Group className="mt-4 pl-0" as={Col} md={4} >
                {updatePhoto ? (
                    <Picture>
                        <img src={imgPreview} alt="imagem escolhida" />
                        <span onClick={removePhoto}>Remover</span>
                    </Picture>
                ) : (
                        <input name="photo" type="file" onChange={(e) => onImageChange(e)} />
                    )}
            </Form.Group>


            <Button variant="primary" type="submit" onClick={(e) => submitForm(e)} disabled={isNotValid()} className="mt-3 mb-4">
                {buttonText()}
            </Button>
        </Form>
    </>)
}

const Picture = styled.div`

    display: flex;
    flex-direction: column;

    img{
        max-width: 400px;
        max-height: 400px;
    }

    span{
        cursor: pointer;
        color: #ccc;
        &:hover{
            color: red
        }
    }


`


export default FormBanner