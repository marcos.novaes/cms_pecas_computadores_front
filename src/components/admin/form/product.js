import React, { useState, useEffect } from 'react'
import { Form, Button, InputGroup, Row, Col, ButtonGroup, ToggleButton } from 'react-bootstrap'
import { FaTimes, FaCheck } from 'react-icons/fa';
import styled from 'styled-components';
import currencyConfig from '../../../config/currency'
import IntlCurrencyInput from 'react-intl-currency-input'
import { createProduct, updateProduct } from '../../../services/admin'

const FormProduct = ({ prod, showT }) => {

    const [form, setForm] = useState({})
    const [updatePhoto, setUpdatePhoto] = useState(false)
    const [imgPreview, setImgPreview] = useState()
    const [isUpdate, setIsUpdate] = useState(false)


    const typeReq = (data) => isUpdate ? updateProduct(prod._id, data) : createProduct(data)


    const handleChange = (e) => {
        const { name, value, checked } = e.target
        const isCheck = name === "status"

        setForm({
            ...form,
            [name]: isCheck ? checked : value
        })
        return;
    }

    const onImageChange = (event) => {
        if (event.target.files && event.target.files[0]) {
            let reader = new FileReader();
            let file = event.target.files[0];
            reader.onloadend = () => {
                setForm({
                    ...form,
                    'photo': file
                });
                setImgPreview(reader.result)
                setUpdatePhoto(true)
            };
            reader.readAsDataURL(file);
        }
    };

    const handlePrice = (event, value, maskedValue) => {
        event.preventDefault();
        setForm({
            ...form,
            "price": value
        })
    };

    const handlePriceDiscount = (event, value, maskedValue) => {
        event.preventDefault();
        const percent = value / form.price * 100;
        const percentAllow = percent >= 100 ? 100 : percent
        setForm({
            ...form,
            discount_price: value,
            discount_price_percent: percentAllow
        })
    };

    const handlePercentDiscount = (attr) => {
        const desc = attr.target.value;

        const valorDesconto = Math.round(form.price * (desc / 100))
        setForm({
            ...form,
            discount_price: form.price - valorDesconto,
            discount_price_percent: desc >= 0 && desc <= 100 ? desc : 0
        })

    };

    const buttonText = () => {
        const text = isUpdate ? "Atualizar" : "Criar"
        return text
    }

    const removePhoto = () => {
        setUpdatePhoto(false)
        setForm({
            ...form,
            photo: ""
        })
    }

    const isNotValid = () => {
        return !(form.title && form.description && form.complete_description && form.price && form.discount_price && form.discount_price_percent && form.photo)
    }

    const clearForm = () => {
        setForm({})
        setImgPreview("")
        setUpdatePhoto(false)
    }

    const submitForm = (e) => {
        e.preventDefault()

        let data = new FormData()


        Object.keys(form)
            .forEach(key => data.append(key, form[key]))

        const config = {
            headers: {
                'Content-type': 'multipart/form-data'
            }
        }

        typeReq(data, config)
            .then((res) => {
                clearForm()
                showT(true)
            })
            .catch((err) => console.log(`Erro ao cadastrar produto.`))

    }


    useEffect(() => {

        if (Object.keys(prod).length > 0) {
            setIsUpdate(true)
            setForm(prod)
            setImgPreview(prod.photo)
            setUpdatePhoto(true)
        }

    }, [prod])

    return (<>
        <Form>
            <Form.Group controlId="formBasicTitle">
                <Form.Label>Titulo</Form.Label>
                <Form.Control type="text" placeholder="Digite o nome" name="title" value={form.title || ""} onChange={(e) => handleChange(e)} />
            </Form.Group>

            <Form.Group controlId="formBasicSDescription">
                <Form.Label>Descrição Simples</Form.Label>
                <Form.Control type="text" placeholder="Digite uma descrição resumida do produto" name="description" value={form.description || ""} onChange={(e) => handleChange(e)} />
            </Form.Group>
            <Form.Group controlId="formBasicCDescription">
                <Form.Label>Descrição Completa</Form.Label>
                <Form.Control type="text" as="textarea" rows="5" placeholder="Digite uma descrição completa do produto" name="complete_description" value={form.complete_description || ""} onChange={(e) => handleChange(e)} />
            </Form.Group>
            <Row>
                <Col>
                    <Form.Group >
                        <Form.Label>Preço</Form.Label>
                        <IntlCurrencyInput className="form-control" currency="BRL" config={currencyConfig} value={form.price || ""} onChange={handlePrice} />
                    </Form.Group>
                </Col>
                <Col>
                    <Form.Group >
                        <Form.Label>Preço com desconto</Form.Label>
                        <IntlCurrencyInput disabled={!form.price} className="form-control" value={form.discount_price || ""} currency="BRL" config={currencyConfig} onChange={handlePriceDiscount} />
                    </Form.Group>
                </Col>
                <Col>
                    <Form.Group >
                        <Form.Label>Desconto (%)</Form.Label>
                        <Form.Control disabled={!form.price} type="number" onChange={handlePercentDiscount} max="100" min="0" name="discount_price_percent" value={form.discount_price_percent || ""} placeholder="Desconto em porcentagem" />

                    </Form.Group>
                </Col>
            </Row>
            <Form.Group className="mt-4 pl-0" as={Col} md={4} >
                {updatePhoto ? (
                    <Picture>
                        <img src={imgPreview} alt="imagem escolhida" />
                        <span onClick={removePhoto}>Remover</span>
                    </Picture>
                ) : (
                        <input name="photo" type="file" onChange={(e) => onImageChange(e)} />
                    )}
            </Form.Group>
            <Form.Group className="mt-4 mb-2">
                <Row>
                    <Col md={1}>
                        <Form.Label >
                            Status
                        </Form.Label>
                    </Col>
                    <ButtonGroup toggle className="mr-3 ml-0">
                        <ToggleButton
                            type="checkbox"
                            variant={Boolean(form.status) ? 'success' : 'danger'}
                            name="status"
                            onChange={(e) => handleChange(e)}
                            checked={Boolean(form.status)}
                        >
                            {Boolean(form.status) ? < FaCheck /> : <FaTimes />}
                            {Boolean(form.status) ? "Ativo" : "Inativo"}
                        </ToggleButton>
                    </ButtonGroup>
                </Row>
            </Form.Group>

            <Button variant="primary" type="submit" onClick={(e) => submitForm(e)} className="mt-3 mb-4" disabled={isNotValid()}>
                {buttonText()}
            </Button>
        </Form>
    </>)
}

const Picture = styled.div`

    display: flex;
    flex-direction: column;

    img{
        max-width: 400px;
        max-height: 400px;
    }

    span{
        cursor: pointer;
        color: #ccc;
        &:hover{
            color: red
        }
    }


`



export default FormProduct