import { Form, Button } from "react-bootstrap"
import React, { useState, useEffect } from 'react'
import { createService, updateService } from '../../../services/admin'

const FormServico = ({ srvc, showT }) => {

    const [form, setForm] = useState({})
    const [isUpdate, setIsUpdate] = useState(false)
    const typeReq = (data) => isUpdate ? updateService(srvc._id, data) : createService(data)

    const handleChange = (e) => {
        setForm({
            ...form,
            [e.target.name]: e.target.value
        })
        return;
    }

    const clearForm = () => {
        setForm({})
    }


    const submitForm = async (e) => {
        e.preventDefault()
        try {
            const result = await typeReq(form)
            console.log(result)
            clearForm()
            showT(true)
        } catch (error) {
            console.log(error.message)
        }
    }

    const buttonText = () => {
        const text = isUpdate ? "Atualizar" : "Criar"
        return text
    }


    const isSubmitValid = () => {
        return form.name && form.description && form.icon
    }

    useEffect(() => {

        if (Object.keys(srvc).length > 0) {
            setIsUpdate(true)
            setForm(srvc)
        }

    }, [srvc])

    return (
        <>
            {/* <Button>Ver</Button> */}
            <Form>
                <Form.Group controlId="formBasicName">
                    <Form.Label>Nome</Form.Label>
                    <Form.Control type="text" placeholder="Digite o nome" name="name" value={form.name || ""} onChange={(e) => handleChange(e)} />
                </Form.Group>

                <Form.Group controlId="formBasicDescription">
                    <Form.Label>Descrição</Form.Label>
                    <Form.Control type="text" placeholder="Digite a descrição" name="description" value={form.description || ""} onChange={(e) => handleChange(e)} />
                </Form.Group>
                <Form.Group controlId="formBasicIcon" >
                    <Form.Label>Icone</Form.Label>
                    <Form.Control type="text" placeholder="O icone que voce quer do lado" name="icon" value={form.icon || ""} onChange={(e) => handleChange(e)} />
                    <Form.Text className="text-muted">
                        Procure por icones em https://react-icons.github.io/react-icons/, clique no desejado e cole aqui.
                     </Form.Text>
                </Form.Group>
                <Button variant="primary" type="submit" onClick={(e) => submitForm(e)} disabled={!isSubmitValid()} >
                    {buttonText()}
                </Button>
            </Form>
        </>
    )
}

export default FormServico