import React, { useState, useEffect } from 'react'
import { Container, Form, Button, Table } from 'react-bootstrap'
import FormInfo from './form/info'
import TableInfo from './table/info'
import { getInfos } from '../../services/admin'

const Info = (props) => {
    const [showTable, setShowTable] = useState(true)

    const [infos, setInfos] = useState([])

    const [infoAtual, setInfoAtual] = useState({})


    const handleCLickShowTable = () => {
        setInfoAtual({})
        setShowTable(!showTable)
    }

    useEffect(() => {
        let pegarInfos = async () => {
            try {
                const infs = await getInfos()
                setInfos(infs.data)
            } catch (error) {
                console.log(error)
            }
        }
        pegarInfos()

        return () => { }
    }, [showTable])

    return (
        <>
            <Container>
                <Button className="mb-4 px-4" variant="outline-success" size="lg" onClick={() => handleCLickShowTable()}>{showTable ? "Criar" : "Ver"}</Button>
                {showTable ?
                    <TableInfo infs={infos} showT={setShowTable} setInfo={setInfoAtual} > </TableInfo> :
                    <FormInfo inf={infoAtual} showT={setShowTable} ></FormInfo>
                }


            </Container>


        </>

    )
}

export default Info