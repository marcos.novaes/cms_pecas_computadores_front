import React, { useState, useEffect } from 'react'
import { Container, Form, Button, Table } from 'react-bootstrap'
import styled from 'styled-components';
import FormServico from './form/servico'
import TableService from './table/service'
import { getServices } from '../../services/admin'

const Servicos = (props) => {
    const [showTable, setShowTable] = useState(true)

    const [services, setServices] = useState([])

    const [serviceAtual, setServiceAtual] = useState({})


    const handleCLickShowTable = () => {
        setServiceAtual({})
        setShowTable(!showTable)
    }

    useEffect(() => {
        let pegarServices = async () => {
            try {
                const servics = await getServices()
                setServices(servics.data)
            } catch (error) {
                console.log(error)
            }
        }
        pegarServices()

        return () => { }
    }, [showTable])



    return (
        <>
            <Container>
                <Button className="mb-4 px-4" variant="outline-success" size="lg" onClick={() => handleCLickShowTable()}>{showTable ? "Criar" : "Ver"}</Button>
                {showTable ?
                    <TableService servics={services} showT={setShowTable} setService={setServiceAtual} ></TableService> :
                    <FormServico srvc={serviceAtual} showT={setShowTable} ></FormServico>
                }


            </Container>


        </>

    )
}

export default Servicos