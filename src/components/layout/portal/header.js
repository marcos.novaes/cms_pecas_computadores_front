import React from 'react'
import { Navbar, Container } from 'react-bootstrap'
import styled from 'styled-components'
import { FaWolfPackBattalion } from 'react-icons/fa'


const styleNav = {
    justifyContent: "center"
}

export default (props) => {
    return (
        <Header>
            <Container>
                <Navbar expand="lg" variant="dark" style={styleNav}>
                    <Navbar.Brand href="#home">
                        <Logo>
                            <FaWolfPackBattalion /> Junin
                            <br />
                            <span>Peças</span>
                        </Logo>
                    </Navbar.Brand>
                </Navbar>
            </Container>
        </Header >
    )
}

const Header = styled.div`
    background-color: #6bb9f0;
    height: 90px;
`

const Logo = styled.div`
    font-size: 30px;
    font-fontWeight: 600;
    margin:0;

    svg{
        color: white;
        font-size: 40px;
    }

    span{
        color:  #22313f;
        margin:0;
        font-size: 20px;
        text-transform: uppercase;
        display: block;
        text-align: center;
        letter-spacing: 4px;
    }

`