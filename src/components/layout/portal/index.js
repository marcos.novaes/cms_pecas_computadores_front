import React from 'react'
import styled from 'styled-components';

import Header from './header'
import Footer from './footer'

const Layout = ({ children }) => {
    return (
        <>
            <HeaderContainer>
                <Header />
            </HeaderContainer>
            <Content>
                {children}
            </Content>
            <FooterContainer>
                <Footer />
            </FooterContainer>
        </>
    )
}



const HeaderContainer = styled.div`
`

const FooterContainer = styled.div`
`

const Content = styled.div`
    min-height: 500px;
`

export default Layout
