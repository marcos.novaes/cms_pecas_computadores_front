import clientHttp from '../config/clientHttp'

const authentication = (data) => clientHttp.post('/login', data)

export {
    authentication
}