import clientHttp from '../config/clientHttp'

//GET
const getServices = () => clientHttp.get('/service')
const getProducts = () => clientHttp.get('/product')
const getInfos = () => clientHttp.get('/info')
const getBanners = () => clientHttp.get('/banner')

// DElELE
const deleteService = (id) => clientHttp.delete(`/service/${id}`)
const deleteProduct = (id) => clientHttp.delete(`/product/${id}`)
const deleteInfo = (id) => clientHttp.delete(`/info/${id}`)
const deleteBanner = (id) => clientHttp.delete(`/banner/${id}`)

// CREATE
const createService = (data) => clientHttp.post(`/service`, data)
const createProduct = (data, config = {}) => clientHttp.post(`/product`, data, config)
const createInfo = (data) => clientHttp.post(`/info`, data)
const createBanner = (data) => clientHttp.post(`/banner`, data)

// UPDATE
const updateService = (id, data) => clientHttp.patch(`/service/${id}`, data)
const updateProduct = (id, data) => clientHttp.patch(`/product/${id}`, data)
const updateInfo = (id, data) => clientHttp.patch(`/info/${id}`, data)
const updateBanner = (id, data) => clientHttp.patch(`/banner/${id}`, data)


export {
    getProducts,
    getServices,
    getInfos,
    getBanners,
    deleteService,
    deleteProduct,
    deleteInfo,
    deleteBanner,
    createService,
    createProduct,
    createInfo,
    createBanner,
    updateService,
    updateProduct,
    updateInfo,
    updateBanner
}